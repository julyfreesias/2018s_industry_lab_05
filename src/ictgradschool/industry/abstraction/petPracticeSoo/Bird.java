package ictgradschool.industry.abstraction.petPracticeSoo;

public class Bird implements IAnimal {


    @Override
    public String greeing() {
        return "tweet tweet";
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public String myName() {
        return "Tweet";
    }

    @Override
    public int numLegs() {
        return 2;
    }
}
