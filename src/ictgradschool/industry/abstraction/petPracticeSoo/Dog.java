package ictgradschool.industry.abstraction.petPracticeSoo;

public class Dog implements IAnimal{
    @Override
    public String greeing() {
        return "Wof Wof";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        return "Bruno";
    }

    @Override
    public int numLegs() {
        return 4;
    }
}
