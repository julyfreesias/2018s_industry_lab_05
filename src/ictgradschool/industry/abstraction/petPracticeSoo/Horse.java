package ictgradschool.industry.abstraction.petPracticeSoo;

import ictgradschool.industry.abstraction.pets.IFamous;

public class Horse implements IAnimal,IFamous {
    @Override
    public String greeing() {
        return "neigh";
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public String myName() {
        return "Mr.Ed";
    }

    @Override
    public int numLegs() {
        return 4;
    }


    @Override
    public String famous() {
        return "PharLap";
    }
}
