package ictgradschool.industry.abstraction.petPracticeSoo;

public class AnimalApp {

    public void start(){
        IAnimal[] animals = new IAnimal[3];
        animals[0]=new Bird();
        animals[1]=new Dog();
        animals[2]=new Horse();

        animalDetails(animals);
    }

    private void animalDetails(IAnimal[] list){

        for(int i=0; i< 3; i++){
            System.out.println(list[i].myName()+" says "+ list[i].greeing());
            if(list[i].isMammal()){
                System.out.println(list[i].myName()+" is a mammal ");
            }else{
                System.out.println(list[i].myName()+" is a non mammal ");
            }
            System.out.println("Did I forget to tell you that I have "+list[i].numLegs()+" legs.");
            if(list[i] instanceof IFamous){
                IFamous other = (IFamous)list[i];
                System.out.println("This is a famous name of my animal type :"+ other.famous());
            }
        }

    }

    public static void main(String[] args) {
        new AnimalApp().start();
    }

}
