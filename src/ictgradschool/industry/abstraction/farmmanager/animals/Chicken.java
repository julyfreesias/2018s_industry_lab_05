package ictgradschool.industry.abstraction.farmmanager.animals;

public class Chicken extends Animal implements IProductionAnimal {
    private final int MAX_VALUE = 300;

    public Chicken() {
        value = 200;
    }


    @Override
    public void feed() {
        if (value < MAX_VALUE) {
            value = value + (MAX_VALUE - value) / 2;
        }


    }

    @Override
    public int costToFeed() {
        return 3;
    }

    @Override
    public String getType() {
        return "Chicken";
    }

    public String toString() {
        return getType() + " - $" + value;
    }

    @Override
    public boolean harvestable() {
        return value == MAX_VALUE;
    }

    @Override
    public int harvest() {
        if (harvestable()) {
            return 5;
        }
        return 0;
    }
}
