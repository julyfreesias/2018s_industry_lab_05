package ictgradschool.industry.abstraction.farmmanager.animals;

public class Pig extends Animal implements IProductionAnimal {

    private final int MAX_VALUE = 500;

    public Pig() {
        value = 300;
    }

    @Override
    public void feed() {
        if (value < MAX_VALUE) {
            value += 50;
        }


    }

    @Override
    public int costToFeed() {
        return 10;
    }

    @Override
    public String getType() {
        return "Pig";
    }

    public String toString() {
        return getType() + " - $" + value;
    }

    @Override
    public boolean harvestable() {
        return value == MAX_VALUE;
    }

    @Override
    public int harvest() {
        if (harvestable()) {
            return 10;
        }
        return 0;
    }
}